function twoNumberSum (array, targetSum){

    let aux = 1;
    let array_twoNumberSum = [];
    
    for (let i=0; i< array.length-1; i++){
        for (let j=aux; j< array.length; j++){
            let sum = array[i] + array[j];
            if (sum == targetSum){
                array_twoNumberSum = [array[i],array[j]];
                return array_twoNumberSum;
            }
        }
        aux++;
    }
    return array_twoNumberSum;
}

const array = [3, 5, -4, 8, 11, 1, -1, 6];
const targetSum = 10;
const result = twoNumberSum(array, targetSum);

if (result.length==2){
    console.log("El array resultante es: [" + result + "]");
}
else{
    console.log("No hay coincidencias");
}
